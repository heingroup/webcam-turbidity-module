# import relevant libraries
import plotly
import plotly.graph_objects as go
import pandas as pd

# read in data [assumes .csv with column headers T_diss or T_nucl, TBZ (M), IPA_percent, Exp_no]
df_nucl = pd.read_csv('data_T_nucl.csv', header=0)
df_diss = pd.read_csv('data_T_diss.csv', header=0)
df_nucl42 = pd.read_csv('data_T_nucl_42.csv', header=0)
df_diss42 = pd.read_csv('data_T_diss_42.csv', header=0)
df_nuclsurf = pd.read_csv('surf_T_nucl.csv', header=0)
df_disssurf = pd.read_csv('surf_T_diss.csv', header=0)


# create figure
fig = go.Figure()

# specify axis data for T_nucl data points
x_nucl = df_nucl['T_nucl'] # temperature data
y_nucl = df_nucl['IPA_percent'] # solvent percent
z_nucl = df_nucl['TBZ (M)'] # solute concentration

# specify axis data for T_diss data points
x_diss = df_diss['T_diss']
y_diss = df_diss['IPA_percent']
z_diss = df_diss['TBZ (M)']

# specify axis data for T_nucl data points for AJK-1-042
x_nucl42 = df_nucl42['T_nucl'] # temperature data
y_nucl42 = df_nucl42['IPA_percent'] # solvent percent
z_nucl42 = df_nucl42['TBZ (M)'] # solute concentration

# specify axis data for T_diss data points for AJK-1-042
x_diss42 = df_diss42['T_diss']
y_diss42 = df_diss42['IPA_percent']
z_diss42 = df_diss42['TBZ (M)']

# specify axis data for T_nucl surface
x_nuclsurf = df_nuclsurf['T_nucl_surf'] # temperature data
y_nuclsurf = df_nuclsurf['IPA_percent_surf'] # solvent percent
z_nuclsurf = df_nuclsurf['TBZ (M)_surf'] # solute concentration

# specify axis data for T_diss surface
x_disssurf = df_disssurf['T_diss_surf']
y_disssurf = df_disssurf['IPA_percent_surf']
z_disssurf = df_disssurf['TBZ (M)_surf']

# create hover template to make mouse overs pretty
hovertemplate= "Temp: <b>%{x:.3f}</b><br>" + \
               "IPA: <b>%{y:.0%}</b><br>" + \
               "TBZ: <b>%{z:.3f} M</b>" + \
               "<extra></extra>"

# GRAPH DATA
# add T_nucl surface
fig.add_trace(go.Mesh3d(x=x_nuclsurf,
                        y=y_nuclsurf,
                        z=z_nuclsurf,
                        opacity=0.5,
                        color='dodgerblue',
                        name='T_nucl (surface)', # text for legend
                        showlegend=True,
                        hovertemplate=hovertemplate
                        ))

# add T_diss surface
fig.add_trace(go.Mesh3d(x=x_disssurf,
                        y=y_disssurf,
                        z=z_disssurf,
                        opacity=0.5,
                        color='orangered',
                        name='T_diss (surface)', # text for legend
                        showlegend=True,
                        hovertemplate=hovertemplate
                        ))

# add T_nucl observed points
fig.add_trace(go.Scatter3d(
    x=x_nucl,
    y=y_nucl,
    z=z_nucl,
    mode='markers',
    name='T_nucl (measured)', # text for legend
    marker=dict(
        size=3,
        color='blue',
        line=dict(width=1,
                  color='DarkSlateGrey'),
        # color = z,  # set color to an array/list of desired values
        opacity=0.8),
    showlegend=True,
    hovertemplate=hovertemplate
))

# add T_diss observed points
fig.add_trace(go.Scatter3d(
    x=x_diss,
    y=y_diss,
    z=z_diss,
    mode='markers',
    name='T_diss (measured)', # text for legend
    marker=dict(
        size=3,
        color='red',
        line=dict(width=1,
                  color='DarkSlateGrey'),
        # color = z,  # set color to an array/list of desired values
        opacity=0.8),
    showlegend=True,
    hovertemplate=hovertemplate
))

# add T_nucl observed points for AJK-1-042
fig.add_trace(go.Scatter3d(
    x=x_nucl42,
    y=y_nucl42,
    z=z_nucl42,
    mode='markers',
    name='T_nucl (measured)', # text for legend
    marker=dict(
        size=3,
        color='turquoise',
        line=dict(width=1,
                  color='DarkSlateGrey'),
        # color = z,  # set color to an array/list of desired values
        opacity=0.8),
    showlegend=True,
    hovertemplate=hovertemplate
))

# add T_diss observed points for AJK-1-042
fig.add_trace(go.Scatter3d(
    x=x_diss42,
    y=y_diss42,
    z=z_diss42,
    mode='markers',
    name='T_diss (measured)', # text for legend
    marker=dict(
        size=3,
        color='maroon',
        line=dict(width=1,
                  color='DarkSlateGrey'),
        # color = z,  # set color to an array/list of desired values
        opacity=0.8),
    showlegend=True,
    hovertemplate=hovertemplate
))

# customize axis range and add labels
fig.update_layout(
    scene=dict(
        xaxis=dict(nticks=10, range=[0, 60]), # nticks approx. number of ticks displayed
        yaxis=dict(tickvals=[0, 0.25, 0.5, 0.75, 1], range=[0, 1]), # tickvals specifed to fit ratios tested
        zaxis=dict(nticks=10, range=[0, z_diss.max()]),
        xaxis_title='Temp (C)',
        yaxis_title='Fraction IPA',
        zaxis_title='[TBZ] (M)'),
        title='<b>3D MSZW Plot [TBZ in IPA:H20]</b>')

# save fig; will overwrite existing fig of same name
fig.write_html("TBZ_MSZW_3Dscatter_surface_lineofbestfit_42.html")

fig.show()